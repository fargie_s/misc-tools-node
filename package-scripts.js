const utils = require('nps-utils');

module.exports = {
  scripts: {
    test: {
      default: {
        script: 'mocha test',
        description: 'Run unit-tests' },
      dev: {
        script: utils.crossEnv('multi="mocha-notifier-reporter=- spec=-" nps "test -R mocha-multi"'),
        description: "Run unit-tests in dev environment (notifier)" },
      watch: {
        script: 'nodemon -w src -w test -x "nps test.dev"',
        description: 'Run unit-tests in dev environment and watch for changes' },
      lint: {
        default: {
          script: './bin/lint',
          description: 'Run the linter' },
        watch: 'nodemon -w src -w test -x \'nps test.lint\'',
        builder: {
          script: './bin/lint --builder',
          description: 'Run the linter with an easy to grep output' },
        badge: {
          script: './bin/mkbadge',
          description: 'Create a linting badge "./badge.svg" (CI)' }
      },
      coverage: {
        default: 'nyc -n src -a -r lcovonly -r text ./node_modules/.bin/_mocha test',
        watch: 'nodemon -w src -w test -x \'nps test.coverage\''
      }
    }
  }
};
