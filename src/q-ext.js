'use strict'; /*jshint -W097 */

const
  _ = require('lodash'),
  q = require('q'),
  stream = require('./stream-ext'),
  debug = require('debug')('q-ext');

class Lock {
  constructor(count) {
    this.count = count;
    this.locked = [];
  }

  wait(value) {
    if (this.count === 0) {
      var defer = q.defer();
      defer.resolve = defer.resolve.bind(defer, value);
      this.locked.push(defer);
      debug('locked(%d)', _.size(this.locked));
      return defer.promise;
    }
    this.count -= 1;
    return value;
  }

  post() {
    if (!_.isEmpty(this.locked)) {
      var defer = this.locked.shift();
      _.defer(defer.resolve);
    }
    else {
      this.count += 1;
    }
  }
}
q.Lock = Lock;

class Throttle {
  constructor(wait) {
    this.wait = wait;
    this.throttled = [];
    this.tick();
    this.timeout = null;
  }

  tick() {
    this.nextCall = this.wait + _.now();
  }

  _enqueue() {
    var defer = q.defer();
    if (_.isEmpty(this.throttled)) {
      this.timeout = setTimeout(this._timeout.bind(this),
        Math.max(this.nextCall - _.now(), 0));
    }
    this.throttled.push(defer);
    return defer.promise;
  }

  _timeout() {
    debug('running throttled');
    this.tick();
    /* run it in next loop */
    var defer = this.throttled.shift();
    _.defer(defer.resolve.bind(defer));
    if (!_.isEmpty(this.throttled)) {
      this.timeout = setTimeout(this._timeout.bind(this),
        Math.max(this.nextCall - _.now(), 0));
    }
  }

  promise() {
    if (_.isEmpty(this.throttled) && (this.nextCall <= _.now())) {
      debug('direct run');
      this.tick();
      return q();
    }
    return this._enqueue();
  }
}
q.Throttle = Throttle;

function _qreduceStream(stream, fun, ret) {
    var prom = q.defer();
    var chain = q(ret);
    var running = false;
    var ended = false;

    function onData() {
      if (running || ended) { return; }

      var data = stream.readable ? stream.read() : null;
      if (_.isNil(data)) { return; }
      else {
        running = true;
        chain = chain.then(_.partial(fun, _, data))
        .tap(() => { running = false; onData(); })
        .catch((err) => { onEnd(); throw err; });
      }
    }

    function onEnd() {
      if (!ended) {
        chain.then(prom.resolve.bind(prom), prom.reject.bind(prom));
        stream.removeListener('end', onEnd);
        stream.removeListener('readable', onData);
        stream.destroy();
      }
      ended = true;
    }

    stream.on('readable', onData);
    stream.once('end', onEnd);
    onData();
    return prom.promise;
}

function _qreduceStreamParallel(count, stream, fun, ret) {
  var prom = q.defer();
  var runners = _.assign([ ], { active: 0, free: [] });
  var ended = false;

  function onData(idx) {
    if ((runners.active >= count) || ended) { return false; }
    else if (_.isNil(idx) &&
      (runners.length === count) &&
      (runners.free.length === 0)) {
      return false;
    }

    var runner;
    var data = stream.readable ? stream.read() : null;
    if (_.isNil(data)) {
      if (!_.isNil(idx)) {
        /* nothing ready, let's flag this one as free */
        runners.free.push(idx);
      }
      return false;
    }

    idx = idx || runners.free.pop();
    if (_.isNil(idx)) {
      /* start a new runner */
      idx = runners.push(q(ret)) - 1;
    }
    runner = runners[idx];

    ++runners.active;
    runner.then(_.partial(fun, _, data))
    .tap(() => {
      if ((--runners.active === 0) && ended) { onEnd(idx); }
      else { onData(idx); }
    })
    .catch((err) => {
       /* only first error stops stream */
      if (prom.promise.isPending()) { prom.reject(err); stream.destroy(); }
      onEnd();
      throw err;
    });

    if ((runners.free.length !== 0) && (stream.readable)) {
      /* try to start a stale worker */
      _.defer(onData);
    }
    return true;
  }

  function onEnd(idx) {
    ended = true;
    if (prom.promise.isPending() && (runners.active === 0)) {
      (runners[idx || 0] || q()).then(prom.resolve.bind(prom));
      stream.destroy();
    }
    stream.removeListener('end', onEnd);
    stream.removeListener('readable', onData);
  }

  stream.on('readable', onData);
  stream.once('end', onEnd);

  /* fill-in the pump */
  for (let i = 0; i < count; ++i) {
    if (!onData()) {
      break;
    }
  }

  return prom.promise;
}

function _qreduce(items, func, offset, ret) {
  if (offset >= items.length) {
    return q(ret);
  }
  else {
    return q.try(func, ret, items[offset], offset, items)
    .then(_.partial(_qreduce, items, func, offset + 1));
  }
}

q.reduceStream = function(stream, func, ret, jobs) {
  jobs = jobs || 1;
  if (jobs === 1) {
    return _qreduceStream(stream, func, ret);
  }
  else {
    return _qreduceStreamParallel(jobs, stream, func, ret);
  }
};

/**
 * create a chain, calling function on provided items
 * @param  {function} func the function to call
 * @param  {} items array of items to call the function on
 * @return {promise}
 *
 * @example
 * q([ 1, 2, 3 ]).reduce(function(result, value) {
 *   return (result || 0) + value;
 * });
 * // is equivalent to
 * funcs.reduce(Q.when, Q(initialVal));
 * var prom = q();
 * [ 1, 2, 3 ].reduce(function(prom, value) {
 *   return prom.then(function(result) {
 *     return (result || 0) + value;
 *   });
 * }, q());
 */
q.reduce = function(items, func, ret) {
  if (items instanceof stream.Readable) {
    return _qreduceStream(items, func, ret);
  }
  else {
    return _qreduce(items, func, 0, ret);
  }
};

q.makePromise.prototype.reduce = function(func) {
  return this.then(_.partial(q.reduce, _, func));
};

/**
 * retry a promise returning function
 * @param  {function} func  the function to call
 * @param  {function} check the check function
 * @param  {integer}  max   maximum retry count (default: 3)
 * @param  {}         value func parameter[description]
 * @return {promise}
 *
 * @details check should return true when a retry should be attempted.
 */
q.retry = function(func, check, max, value) {
  if (_.isNil(max)) { max = 3; }
  return q(value)
  .then(func)
  .catch(function(err) {
    if ((max > 0) && check(err)) {
      return q.retry(func, check, max - 1, value);
    }
    throw err;
  });
};

q.makePromise.prototype.retry = function(func, check, max) {
  return this.then(_.partial(q.retry, func, check, max));
};

/**
 * Bind a native method using its name
 *
 * All arguments must be bound
 *
 * @param  {object} object the object to bind
 * @param  {string} methodName the method to bind
 * @param  {} args arguments to bind
 * @return {promise}
 */
function nmbind(object, methodName, ...args) {
  return _.ary(q.ninvoke.bind(null, object, methodName, ...args), 0);
}
q.nmbind = nmbind;

module.exports = q;
