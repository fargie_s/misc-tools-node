const
  chai = require('chai'),
  dirtyChai = require('dirty-chai');

/*
  dirty-chai forces assertion to use functions rather than accessors:
  expect(null).to.exist --> expect(null).to.exist()
*/
chai.use(dirtyChai);

try {
  const sinonChai = require('sinon-chai'); /* eslint-disable-line global-require */
  chai.use(sinonChai);
}
catch(e) { /* eslint-disable-line no-empty */
}

module.exports = chai;
