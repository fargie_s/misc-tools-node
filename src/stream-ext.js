
const
  _ = require('lodash');

var stream = require('stream');

/**
 * @brief chunks streams of objects
 * @extends stream
 */
class ChunkedStream extends stream.Duplex {
  constructor(options, chunkSize) {
    if (_.isInteger(options)) {
      chunkSize = options;
      options = null;
    }
    options = _.defaults(options, { objectMode: true });
    super(options);

    this._chunkSize = chunkSize;
    this._buffer = new Array(this.chunkSize);
    this._idx = 0;
    this._isFinished = false;

    this.once('finish', () => {
      this._isFinished = true;
      this._next = null;
      this._send();
    });
    this.once('end', () => this.destroy());
  }

  _send() {
    if (this._idx <= 0) {
      if (this._isFinished) {
        return this.push(null);
      }
      return true;
    }
    var buff = this._buffer;
    this._buffer = new Array(this.chunkSize);
    this._idx = 0;
    return this.push(buff);
  }

  _write(chunk, encoding, callback) {
    this._buffer[this._idx++] = chunk;
    if (this._idx >= this._chunkSize) {
      if (this._send()) {
        return callback();
      }
      else {
        this._next = callback;
      }
    }
    else {
      return callback();
    }
  }

  _read() {
    if (this._isFinished) {
      _.unset(this, '_next');
      this._send();
    }
    if (this._idx >= this._chunkSize) {
      this._send();
    }
    if (this._next) {
      var next = this._next;
      this._next = null;
      return next();
    }
  }
}
stream.ChunkedStream = ChunkedStream;

module.exports = stream;
