'use strict';

const
  { it, describe } = require('mocha'),
  { expect } = require('../src/chai-ext'),
  sinon = require('sinon'),
  _ = require('../src/lodash-ext');

describe('lodash-ext', function() {
  it('exports lodash', function() {
    expect(_).equal(require('lodash')); // eslint-disable-line global-require
  });

  it('push element in an array', function() {
    expect(_.push(null, 1)).deep.equal([ 1 ]);

    var array = [ 1 ];
    expect(_.push(array, 2)).deep.equal([ 1, 2 ]);
    expect(array).deep.equal([ 1, 2 ]);

    /* 12 is not an array, an array with element is created */
    expect(_.push(12, 1)).deep.equal([ 1 ]);
  });

  it('bind functions', function() {
    var spy = sinon.spy();
    _.fbind(spy, 12)(13);
    expect(spy).to.be.calledWithExactly(12, 13);
    spy.resetHistory();

    _.fbind0(spy, 12)(13);
    expect(spy).to.be.calledWithExactly(12);
  });

  it('bind methods', function() {
    var object = { foo: _.noop };
    sinon.spy(object, 'foo');
    _.bind0(object.foo, object, 12)(13);
    expect(object.foo).to.be.calledWithExactly(12);
    object.foo.resetHistory();

    _.mbind(object, 'foo', 12)(13);
    expect(object.foo).to.be.calledWithExactly(12, 13);
    object.foo.resetHistory();

    _.mbind0(object, 'foo', 12)(13);
    expect(object.foo).to.be.calledWithExactly(12);
    object.foo.resetHistory();
  });

});
