
const
  { describe, it } = require('mocha'),
  stream = require('../src/stream-ext'),
  _ = require('lodash'),
  { expect } = require('../src/chai-ext');

describe('stream-ext', function() {
  it('exports ext streams', function() {
    expect(stream.ChunkedStream).to.exist();
  });

  it('can chunk a stream', function(done) {
    var st = new stream.ChunkedStream(3);
    var data = [];

    st.on('data', function(d) { data.push(d); });
    st.on('end', function() {
      expect(data).deep.equal([ [ 1, 2, 3 ], [ 4, 5 ] ]);
      done();
    });

    st.resume();
    for (let i = 1; i <= 5; ++i) {
      st.write(i);
    }
    st.end();
  });

  it('can chunk in pipes', function(done) {
    var input = new stream.PassThrough({ objectMode: true });
    var st = new stream.ChunkedStream(3);
    input.pipe(st);
    var data = [];

    st.on('data', function(d) { data.push(d); });
    st.on('end', function() {
      expect(data).deep.equal([ [ 0, 1, 2 ], [ 3, 4 ] ]);
      done();
    });

    _.times(5, (i) => input.write(i));
    input.end();
  });

  it('finishes with only complete chunks', function(done) {
    var input = new stream.PassThrough({ objectMode: true });
    var st = input.pipe(new stream.ChunkedStream(3));
    var data = [];

    st.on('data', function(d) { data.push(d); });
    st.on('end', function() {
      expect(data).deep.equal([ [ 0, 1, 2 ], [ 3, 4, 5 ] ]);
      done();
    });

    _.times(6, (i) => input.write(i));
    input.end();
  });

});
