
const
  { describe, it } = require('mocha'),

  q = require('../src/q-ext'),
  stream = require('../src/stream-ext'),
  { expect } = require('../src/chai-ext');


describe('q-ext', function() {

  describe('reduce streams', function() {
    it('can reduce streams', function() {
      var st = stream.PassThrough({ objectMode: true });

      for (let i = 1; i <= 7; ++i) {
        st.push(i);
      }
      st.push(2);
      st.push(null);

      return q.reduce(st, function(res, item) {
        return res + item;
      }, 12)
      .then(function(ret) { expect(ret).equal(42); });
    });

    it('can reduce streams in parallel', function() {
      var st = stream.PassThrough({ objectMode: true });
      var running = 0;
      var maxRunning = 0;

      for (let i = 1; i <= 20; ++i) {
        st.push(i);
      }
      st.push(null);

      return q.reduceStream(st, function(res, value) {
        res.value += value;

        if (++running > maxRunning) {
          maxRunning = running;
        }
        return q.delay(10)
        .then(() => { --running; return res; });

      }, { value: 10 }, 10)
      .then(function(ret) {
        expect(ret).deep.equal({ value: 220 });
        expect(maxRunning).to.equal(10);
      });
    });

    it('can reduce a slow stream in parallel', function() {
      var st = stream.PassThrough({ objectMode: true });
      st.push(1);
      st.push(1);

      var prom = q();
      prom
      .delay(10).then(() => st.push(1))
      .delay(10).then(() => st.push(1))
      .delay(10).then(() => st.push(1))
      .delay(10).then(() => st.push(1))
      .delay(10).then(() => st.push(null));

      return q.reduceStream(st, function(res, value) {
        res.value += value;
        return res;
      }, { value: 0 }, 2)
      .then(function(ret) {
        expect(ret).deep.equal({ value: 6 });
      });
    });
  });
});
