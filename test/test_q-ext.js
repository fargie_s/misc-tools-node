
const
  { describe, it } = require('mocha'),
  q = require('../src/q-ext'),
  _ = require('lodash'),
  { expect } = require('chai');

class MFBind0Test {
  foo(bound, callback) {
    expect(bound).equal('bound');
    callback(null, 'result');
  }
}

describe('q-ext', function() {
  it('exports q', function() {
    expect(q).equal(require('q')); // eslint-disable-line global-require
  });

  describe('reduce', function() {
    it('can reduce static array', function() {
      return q.reduce([1, 2, 3], function(res, item, key, values) {
        expect(item).equal(key + 1);
        expect(values).property(key).deep.equal(item);
        return item + (_.isNil(res) ? 0 : res);
      })
      .then(function(ret) { expect(ret).equal(6); });
    });

    it('can reduce promise', function() {
      return q([1, 2, 3]).reduce(function(res, item, key, values) {
        expect(item).equal(key + 1);
        expect(values).property(key).deep.equal(item);
        return item + (_.isNil(res) ? 0 : res);
      })
      .then(function(ret) { expect(ret).equal(6); });
    });

    it('can reduce varrying array', function() {
      return q([1, 2]).reduce(function(res, item, key, values) {
        if (item <= 2) {
          values.push(_.size(values) + 1);
        }
        return res ? (res + item) : item;
      })
      .then((ret) => expect(ret).equal(10));
    });
  });

  describe('mbind', function() {
    it('can mbind a class', function() {
      var c = new MFBind0Test();
      return q()
      .then(q.nmbind(c, 'foo', 'bound'))
      .then(function(value) { expect(value).equal('result'); });
    });
  });

  describe('Lock', function() {
    it('can lock a promise', function() {
      var lock = new q.Lock(2);
      expect(lock).property('count').equal(2);

      return q(12)
      .then(lock.wait.bind(lock))
      .then(function(value) {
        expect(lock).property('count').equal(1);
        expect(value).equal(12);
      })
      .then(lock.wait.bind(lock))
      .then(function() {
        var counter = 0;
        var prom = lock.wait()
        .then(function() { counter += 1; });

        expect(counter).equal(0);
        lock.post();
        return prom
        .then(function() {
          expect(lock).property('count').equal(0);
          expect(counter).equal(1);
        });
      })
      .then(lock.post.bind(lock))
      .then(lock.post.bind(lock))
      .then(function() {
        expect(lock).property('count').equal(2);
      });
    });
  });

  describe('Throttle', function() {
    it('can Throttle a function', function() {
      var throttle = new q.Throttle(100); /* 100 ms wait time */

      var count = 0;
      var prom = throttle.promise()
      .then(() => { count += 1; })
      .then(throttle.promise.bind(throttle))
      .then(() => { count += 1; });

      expect(count).equal(0);
      return q().delay(150)
      .then(function() {
        expect(count).equal(1);
        return prom;
      })
      .then(() => { expect(count).equal(2); });
    });

    it('runs directly when possible', function() {
      var throttle = new q.Throttle(100);

      return q.delay(100)
      .then(function() {
        expect(throttle.promise().inspect())
        .property('state').equal('fulfilled');
      });
    });
  });

  describe('retry', function() {
    it('retries max 3 times', function() {
      var ret = 0;
      return q.retry(
        () => { ret += 1; throw 'error'; }, () => { return true; })
      .then(
        () => { throw 'should fail'; },
        (err) => {
          expect(err).equal('error');
          expect(ret).equal(4);
        });
    });

    it('retries as much as I want', function() {
      var ret = 0;
      return q.retry(
        () => { ret += 1; throw 'error'; }, () => { return true; }, 10)
      .then(
        () => { throw 'should fail'; },
        (err) => {
          expect(err).equal('error');
          expect(ret).equal(11);
        }
      );
    });

    it('does not retry on success', function() {
      return q.retry(
        () => { return 1; }, () => { return true; })
      .then((ret) => { expect(ret).equal(1); });
    });

    it('stops trying when I want', function() {
      var ret = 0;
      return q.retry(
        () => { ret += 1; throw ret; }, (err) => { return err !== 4; }, 10)
      .then(
        () => { throw 'should fail'; },
        (err) => {
          expect(err).equal(4);
          expect(ret).equal(4);
        }
      );
    });

    it('handles a failing check', function() {
      return q.retry(() => { throw 42; }, () => { throw 24; })
      .then(
        () => { throw 'should fail'; },
        (err) => {
          expect(err).equal(24);
        }
      );
    });

    it('retries a promise', function() {
      var ret = 0;
      return q(40)
      .retry(
        (value) => {
          ret += 1;
          if (value === 40) { throw 'error'; }
        },
        () => { return true; }
      )
      .then(
        () => { throw 'should fail'; },
        (err) => {
          expect(ret).equal(4);
          expect(err).equal('error');
        }
      );
    });
  });
});
